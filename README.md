# Test of CICD pipeline
  * Training linear regression model.
  * Upload latest linear regression model and related files to package registry.

# How to use latest model
  1. Prepare python environment via `requirements.txt`
  2. Prepare data for model prediction `new_data.csv`
  3. Download latest model:
```
curl --header 'Private-Token: <personal-access-token>' ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/ci-upload-package/$PACKAGE_VERSION/FILENAME --output FILENAME

Example: curl --header 'Private-Token: <personal-access-token>' https://gitlab.com/api/v4/projects/38714692/packages/generic/lr-ci-test/0.0.3/model.joblib --output model.joblib
```    
  4. Proceeding prediction through execute `run-lr.py`

> python version: 3.8.11
