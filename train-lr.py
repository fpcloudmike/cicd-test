#!/usr/bin/env python
# coding: utf-8

import joblib
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score

# create model
X = np.array([[1, 1], [1, 2], [2, 2], [2, 3],[2,3]])
# y = 1 * x_0 + 2 * x_1 + 3
y = np.dot(X, np.array([1, 2])) + 3
reg = LinearRegression().fit(X, y)
y_pred = reg.predict(X)
# 模型預測成效
data_mae = mean_absolute_error(y, y_pred)
data_mse = mean_squared_error(y, y_pred)
data_r2 = r2_score(y, y_pred)
print('mean absolute error=', data_mae)
print('mean square error=', data_mse)
print('r2 score=', data_r2)

joblib.dump(reg,'./model.joblib')
