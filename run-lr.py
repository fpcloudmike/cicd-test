#!/usr/bin/env python
# coding: utf-8

# run model
import joblib
import pandas as pd


lr_model = joblib.load('./model.joblib')
## load new data
new_data = pd.read_csv("new_data.csv").to_numpy()
print(new_data)
## predict result
# new_predict = np.array(new_data)
result = lr_model.predict(new_data)
print(result)
